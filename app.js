const express = require('express')
const app = express()
const cors = require('cors')
const expressip = require('express-ip')
require('dotenv').config()
require('./src/db')
const PORT = process.env.PORT
const userModule = require('./src/user')
const culturalModule = require('./src/cultural')
const planModule = require('./src/plan')
const employeeModule = require('./src/employee')
const path = require('path');
const multer = require('multer')
multer({dest:"uploads/"})

app.use(cors())
app.use(expressip().getIpInfoMiddleware)
app.use("/uploads",express.static("uploads"));
app.use(express.static(path.join(__dirname,"uploads")));

app.use(express.json())

app.use('/api/v1/user',userModule)
app.use('/api/v1/user/culturalsettings', culturalModule)
app.use('/api/v1/plan', planModule)
app.use('/api/v1/employee',employeeModule)

app.listen(PORT, ()=> console.log('server started'))

