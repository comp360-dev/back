const express = require('express')
const model = require('./models/user.model')
const session = require('./models/sessions')
const cultural = require('./models/cultural.model')
const resp = require('./resp')
const router = express.Router()


//endpoints

router.post('/create', (req,res) => createModel(req,res))
router.get('/read/:token', (req,res) => readModel(req,res))
router.patch('/update/:token', (req,res) => updateModel(req,res))

//middlewares


//controlls

const createModel = async (req,res) => {session.findOne({token: req.body.user_id})
                                        .then(data => new cultural(Object.assign(req.body,{user_id: data.user_id})).save())
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}

const readModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => cultural.findOne({user_id: data.user_id}))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}    
                                        
const updateModel = async (req, res) => { session.findOne({token: req.params.token})
                                        .then(data => cultural.findOneAndUpdate({user_id: data.user_id},req.body,{new: true}))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}                                        


                     



module.exports = router