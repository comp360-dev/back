const mongoose = require('mongoose')
mongoose.connect(process.env.DB, {useNewUrlParser: true,useCreateIndex: true,useUnifiedTopology: true})
const db = mongoose.connection
db.once('open', ()=> console.log('connected'))
db.on('error', () => console.log('Failed to connect'))