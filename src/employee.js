const express = require('express')
const model = require('./models/user.model')
const session = require('./models/sessions')
const employee = require('./models/employee.model')
const plan = require('./models/plan.model')
const resp = require('./resp')
const router = express.Router()
const multer = require('multer');
const fs = require("fs");
const path = require("path");
const excelToJson = require('convert-excel-to-json');

//endpoints

router.post('/upload/:token', (req,res) => createModel(req,res))
router.get('/read/:token', (req,res) => readModel(req,res))
router.patch('/update/:token', (req,res) => updateModel(req,res))
router.post('/fileupload/:token', (req,res,next) =>uploadxls(req,res,next),(req,res) => createexeldata(req,res))



//middlewares
//********  file upload  *********
const storage = multer.diskStorage({
    destination: (req, file, cb) => { cb(null, './uploads'); },
    filename: (req, file, cb) => { cb(null, Date.now() + file.originalname); },
  });
  const upload = multer({ storage: storage }).single('file');
  function errorHandler(res, dat) { res.json({ status: 'error', data: dat }) }
  
  const uploadxls = function (req, res, next) {
    upload(req, res, err => { (err) ? errorHandler(res, err) : setpath(req, res, next) })
  }
// ************ *******************

const sessionDecoder = async (req,res) => {
    return new Promise((resolve,reject) => {
        session.findOne({token: req.params.token})
    then(data => {
        if(data === null) {
            reject({message: 'Session expired'})
        } else {
            resolve(data)
        }
    })
    .catch(err => {
       reject(err) 
    })
    })
}


//controlls

const createModel = async (req,res) => { sessionDecoder(req,res)
                                        .then(data )
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}

const readModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => cultural.findOne({user_id: data.user_id}))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}    
                                        
const updateModel = async (req, res) => { session.findOne({token: req.params.token})
                                        .then(data => cultural.findOneAndUpdate({user_id: data.user_id},req.body,{new: true}))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))} 
const createexeldata = async (req,res) => {excel(req.body).then(data => {resp.success(res,data)}).catch(err => {resp.error(res,err)})}
                                 
// services
const excel = async (data) => {return data}
// file upload

async function setpath(req, res, next) {
    const imgs = req.file;
    if (imgs){ 
        filepath = `${process.env.HOST_NAME}/${imgs.path}`;
        user = await session.findOne({token: req.params.token})
        console.log(user)
        result = await importExcelData2MongoDB(imgs.path,user.user_id);
    }
    req.body = result
    next()
  }
  // -> Import Excel File to MongoDB database
const importExcelData2MongoDB = async function(filePath,id){
    try{
    const result = excelToJson({
        sourceFile: filePath,
        header:{
            rows: 1
                },
        columnToKey: {
            '*': '{{columnHeader}}'
                }
    });
    const final_output = await inserdata(result.Sheet1,id)
    // await fs.unlinkSync(filePath);
    // return final_output
    // console.log(result)
    return final_output
}
catch(e)
{
    console.log(e,'errrr')
    throw e
}
}
//insert data to db
const inserdata = async function(array,id){
    try{
    const myplan = await plan.findOne({user_id:id})
    const planid = myplan.plan_id
    await employee.deleteMany({"plan_id":planid});

    // arrayObj.map(({ Employee Id, Salary }) => ({ emp_id: stroke, yourNewKey2: key2 }));
    const mydata = array.map((x)=>{
        var y={...x};        
        y['emp_id']=x['Employee Id'];delete y['Employee Id'];
        y['first_name']=x['First name'];delete y['First name'];
        y['last_name']=x['Last name'];delete y['Last name'];
        y['job_title']=x['Job Title'];delete y['Job Title'];
        y['grade']=x['Grade'];delete y['Grade'];
        y['hire_date']=x['Hire Date'];delete y['Hire Date'];
        y['department']=x['Department'];delete y['Department'];
        y['salary_min']=x['Salary Min'];delete y['Salary Min'];
        y['salary_mid']=x['Salary Mid'];delete y['Salary Mid'];
        y['salary_max']=x['Salary Max'];delete y['Salary Max'];
        y['salary']=x['Salary'];delete y['Salary'];
        y['performance_rating']=x['Performance Rating'];delete y['Performance Rating'];
        y['email']=x['Email'];delete y['Email'];
        y['gender']=x['Gender'];delete y['Gender'];
        y['dob']=x['DoB'];delete y['DoB'];
        y['emp_status']=x['Emp Status'];delete y['Emp Status'];
        y['emp_type']=x['Emp Type'];delete y['Emp Type'];
        y['supervisor_id']=x['Supervisor Id'];delete y['Supervisor Id'];
        y['promotion_eligibility']=x['Promotion Eligibility'];delete y['Promotion Eligibility'];

        let newobj = y
        // y['salary_mid']=x['Salary Mid'];delete y['Employee Id'];
        // const {emp_id,first_name,last_name,job_title,grade,hire_date,department,salary_min,salary_mid,salary_max,salary,performance_rating ,email,gender,dob,emp_status,emp_type, ...other} = newobj
        // const {email,gender,dob,emp_status,emp_type, ...other} = newobj
        o = y
        exclude = new Set(['emp_id','first_name','last_name','job_title','grade','hire_date','department','salary_min','salary_mid','salary_max','salary','performance_rating' ,'email','gender','dob','emp_status','emp_type','supervisor_id'])
        ress = Object.fromEntries(Object.entries(o).filter(e => !exclude.has(e[0])))

        y.extras=ress
        return y
        })
        // for(i=0;i<mydata.length;i++){
        //     await new employee(Object.assign(mydata[i],{plan_id:planid})).save()
        // }
       
         await Promise.all(mydata.map(async (i) => {
         new employee(Object.assign(i,{plan_id:planid})).save()}));
        
    return mydata
}
    catch(e){
      throw e
    }
    }

                     



module.exports = router