const mongoose = require('mongoose')
const employee = mongoose.model('employee',{
    plan_id: {type: String},
    emp_id: {type: String},
    first_name: {type: String},
    last_name: {type: String},
    job_title: {type: String},
    grade: {type: Number},
    hire_date: {type: Date},
    department: {type: String},
    salary_min: {type: Number},
    salary_mid: {type: Number},
    salary_max: {type: Number},
    salary: {type: Number},
    performance_rating: {type: Number},
    email: {type: String},
    gender: {type: String},
    dob: {type: Date},
    emp_status: {type: String},
    emp_type: {type: String},
    supervisor_id: {type: Number},
    extras: {type: Object},
    bonus_list: {type: Array},
    date: {type: Date, default: Date.now()},
    plan_id:{type:Number}
}) 

module.exports = employee