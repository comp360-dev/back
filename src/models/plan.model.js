const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

    const planSchema = Schema({
    is_active: {type: Boolean, required: true, default: true},
    user_id: { type: String, required: true},
    cycle_type: { type: String, required: true},
    cycle_from: { type: Date, required: true},
    cycle_to: { type: Date, required: true},
    eligibility_type: { type: String, required: true},
    eligibility_date: { type: Date},
    eligibility_percentage: { type: Number},
    salary_component: { type: Boolean},
    salary_component_list: { type: Array},
    global_currency: { type: Boolean},
    global_currency_list: { type: Array},
    prorate: { type: Boolean},
    prorate_unit: { type: String},
    off_cycle_prorate: { type: Boolean},
    off_cycle_prorate_unit: { type: String},
    parity: { type: String},
    merit_guidline:{ type: Boolean},
    supervisor_validation:{ type: String},
    split_recommendation: { type: Boolean},
    calculation_split_recommendation: { type: String},
    matrix_recommendation:{ type: Boolean},
    calculation_matrix_recommendation: { type: String},
    paygroups: { type: Boolean},
    paygroups_name: { type: String},
    enable_recommendation: { type: Boolean},
    pay_range_min: { type: String},
    pay_range_max:{ type: String},
    enable_bonus_incentives: { type: Boolean},
    use_diff_bonus_incentives: { type: Boolean},
    basis_of_bonus:{ type: String},
    bonus_list : { type: Array},
    merit_group_list: { type: Array},
    bonus_group_list: { type: Array},
    budget_value:{ type: Number},
    budget_unit:{ type: String},
    hold_back: { type: Boolean},
    hold_back_value:{ type: Number},
    hold_back_unit:{ type: String},
    statutory_increment:{ type: Boolean},
    employee_group:{ type: String},
    increment_value:{ type: Number},
    increment_unit:{ type: String},
    plan_id:{ type: Number}
})
autoIncrement.initialize(mongoose.connection);
planSchema.plugin(autoIncrement.plugin, {
  model: 'myplan', // collection or table name in which you want to apply auto increment
  field: 'plan_id', // field of model which you want to auto increment
  startAt: 1, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
}); 

const plan = mongoose.model('plans', planSchema);
module.exports = plan