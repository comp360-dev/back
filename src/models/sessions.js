const mongoose = require('mongoose')
const model = mongoose.model('sessions', {
    user_id: { type: String, required: true },
    token: { type: String, required: true },
    date: { type: Date, default: Date.now(), expires: 3600 }
})

module.exports = model