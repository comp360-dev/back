const mongoose = require('mongoose')
const model = mongoose.model('users',{
    username: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    fullname: {type: String},
    user_type: {type: String, default: 'hradmin'},
    date: {type: Date, default: Date.now()}
}) 

module.exports = model