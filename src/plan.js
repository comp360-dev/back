const express = require('express')
const user = require('./models/user.model')
const session = require('./models/sessions')
const plan = require('./models/plan.model')
const resp = require('./resp')
const router = express.Router()

//endpoints

router.post('/create/:token', (req,res) => createModel(req,res))
router.get('/get/:token', (req,res) => readModel(req,res))
router.patch('/update/:token', (req,res) => updateModel(req,res))
router.delete('/delete/:token', (req,res) => deleteModel(req,res))

//controlls
const createModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => setPlan(data,req,res))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}

const readModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => findPlan(data))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}  
                                        
const updateModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => plan.findOneAndUpdate({user_id: data.user_id},req.body,{new: true}))
                                        .then(data => resp.success(res,data))
                                        .then(err => resp.error(res,err.message))} 

const deleteModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => plan.findOneAndUpdate({user_id: data.user_id, is_active: true},{is_active: false},{new: true}))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}                                                                                        
                                        
                                        


// additional checks

const setPlan = async (data,req,res) => {
    return new Promise((resolve,reject) => {
        if(data === null) {
            reject({message: 'Session expired'})
            return
        }
        plan.find({user_id: data.user_id, is_active: true})
        .then(x => {
            if(x.length > 0) {
                reject({message: 'Plan is already created for this user'})
                return
            }
            return x
        })
        .then(y => new plan(Object.assign(req.body,{user_id: data.user_id})).save())
        .then(z => resolve(z))
        .catch(err => reject(err))
    })
}

const findPlan = async (data) => {
    return new Promise((resolve,reject) => {
        if(data === null) {
            reject({message: 'Session expired'})
            
        } else {
            plan.findOne({user_id: data.user_id, is_active: true})
            .then(x => {
                if(x === null) {
                    reject({message: 'No plan for this user'})
                } else {
                    resolve(x)
                }
            })
            .catch(err => reject(err))
        }
        
    })
}

//services



module.exports = router