const express = require('express')
const model = require('./models/user.model')
const session = require('./models/sessions')
const resp = require('./resp')
const md5 = require('md5')
const hash = require('hash-generator')
const axios = require('axios')
const router = express.Router()
const ipfinder = require('request-ip')

//endpoints

router.post('/create', (req,res) => registerModel(req,res))
router.get('/read/:token', (req,res) => readModel(req,res))
router.patch('/update/:token', (req,res) => updateModel(req,res))
router.post('/login', (req,res) => loginModel(req,res))
router.get('/islogin/:token', (req,res) => isloginModel(req,res))
router.get('/list', (req,res,next) => admin(req,res,next),(req,res) => listModel(req,res))
router.get('/admin/read/:id', (req,res,next) => admin(req,res,next),(req,res) => readModelByid(req,res))
router.patch('/admin/update/:id', (req,res,next) => admin(req,res,next),(req,res) => updateUserByid(req,res))
router.get('/info', (req,res) => getInfo(req,res))

//middlewares

const admin = async (req,res,next) => { (req.headers.secret === '@dm!n') ? next(): resp.error(res,'Unauthenticated')}

//controlls
const loginModel = async (req,res) => { model.findOne(Object.assign(req.body,{password: await md5(req.body.password)}))
                                        .then(data => new session({user_id: data._id,token: hash(64)}).save())
                                        .then(data => resp.success(res,data.token))
                                        .catch(err => resp.error(res,err.message))}

const isloginModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => resp.success(res,true))
                                        .catch(err => resp.error(res,false))}  

const registerModel = async (req,res) => { new model(Object.assign(req.body,{password: await md5(req.body.password)})).save()
                                            .then(data => new session({user_id: data._id, token: hash(64)}))
                                            .then(data => resp.success(res,data.token))
                                            .catch(err => resp.error(res,err.message))}   
                                            
const readModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => model.findById(data.user_id))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}  

const readModelByid = async (req,res) => { model.findById(req.params.id)
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}   
                                        
const updateModel = async (req,res) => { session.findOne({token: req.params.token})
                                        .then(data => model.findByIdAndUpdate(data.user_id,req.body,{new: true}))
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}   

const updateUserByid = async (req,res) => { model.findByIdAndUpdate(req.params.id,req.body,{new: true})
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}   
                                        
const listModel = async (req,res) => { model.find(req.query.filter)
                                        .limit(parseInt(req.query.limit))
                                        .skip(parseInt(req.query.page)-1)
                                        .sort({date: -1})
                                        .then(data => resp.success(res,data))
                                        .catch(err => resp.error(res,err.message))}   
                                        
const getInfo = async (req,res) => { 
    if(!req.ipInfo.error) {
        axios.get(`https://ipapi.co/${req.ipInfo.ip}/json/`)
        .then(data => resp.success(res,data.data))
        .catch(err => resp.error(res,err.message))
    }
   
}


module.exports = router